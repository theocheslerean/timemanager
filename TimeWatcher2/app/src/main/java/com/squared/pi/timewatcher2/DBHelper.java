package com.squared.pi.timewatcher2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Theodor on 14.05.2017.
 */

public class DBHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SubjectTimeManagement.db";

    private static final String CREATE_TABLE_SUBJECTS =
            "CREATE TABLE " + FeederReaderContract.FeedEntry.TABLE_SUBJECTS + " (" +
                    FeederReaderContract.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeederReaderContract.FeedEntry.SUBJECT_COLUMN_NAME + " TEXT," +
                    FeederReaderContract.FeedEntry.SUBJECT_COLUMN_EXPECTED_TIME + " INTEGER," +
                    FeederReaderContract.FeedEntry.SUBJECT_COLUMN_DONE_TIME + " INTEGER)";

    private static final String CREATE_TABLE_WORKENTRIES =
            "CREATE TABLE " + FeederReaderContract.FeedEntry.TABLE_WORKENTRIES + " (" +
                    FeederReaderContract.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeederReaderContract.FeedEntry.WORKENTRY_START_DATE + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_END_DATE + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_START_TIME + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_END_TIME + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_WORK_TIME + " INTEGER," +
                    FeederReaderContract.FeedEntry.WORKENTRY_MODIFIED + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_MESSAGE + " TEXT," +
                    FeederReaderContract.FeedEntry.WORKENTRY_SUBJECT + " INTEGER)";
            ;

    private static final String DELETE_TABLE_SUBJECTS =
            "DROP TABLE IF EXISTS " + FeederReaderContract.FeedEntry.TABLE_SUBJECTS;

    private static final String DELETE_TABLE_WORKENTRIES =
            "DROP TABLE IF EXISTS " + FeederReaderContract.FeedEntry.TABLE_WORKENTRIES;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d("DBHelper", "onCreate() started");
        db.execSQL(CREATE_TABLE_WORKENTRIES);
        db.execSQL(CREATE_TABLE_SUBJECTS);

        Log.d("DBHelper", "onCreate() finished");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_TABLE_SUBJECTS);
        db.execSQL(DELETE_TABLE_WORKENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cV = new ContentValues();

        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_NAME, subject.getName());
        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_EXPECTED_TIME, subject.getTarget());
        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_DONE_TIME, subject.getCurrent());
        return db.insert(FeederReaderContract.FeedEntry.TABLE_SUBJECTS, null, cV);
    }


    public long insertWorkEntry(Subject subject, WorkEntry workEntry) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cV = new ContentValues();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);

        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_START_DATE, dateFormat.format(workEntry.getStartDate()));
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_END_DATE, dateFormat.format(workEntry.getEndDate()));
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_START_TIME, dateFormat.format(workEntry.getStartTime()));
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_END_TIME, dateFormat.format(workEntry.getEndTime()));
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_MODIFIED, dateFormat.format(workEntry.getModified()));
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_MESSAGE, workEntry.getMessage());
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_WORK_TIME, workEntry.getWork());
        cV.put(FeederReaderContract.FeedEntry.WORKENTRY_SUBJECT, subject.getID());

        return db.insert(FeederReaderContract.FeedEntry.TABLE_WORKENTRIES, null, cV);
    }

    public long updateSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cV = new ContentValues();
        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_NAME, subject.getName());
        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_EXPECTED_TIME, subject.getTarget());
        cV.put(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_DONE_TIME, subject.getCurrent());
        return db.update(FeederReaderContract.FeedEntry.TABLE_SUBJECTS, cV, "_id=" + subject.getID(), null);
    }

    public ArrayList<Subject> getAllSubjects() {
        Log.d("DBHelper", "start getAllSubjects()");
        ArrayList<Subject> list = new ArrayList<Subject>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + FeederReaderContract.FeedEntry.TABLE_SUBJECTS, null);
        c.moveToFirst();

        Log.d("DBHelper", "everything set up");

        while(!c.isAfterLast()) {
            Subject subject = new Subject(
                    c.getInt(c.getColumnIndex(FeederReaderContract.FeedEntry._ID)),
                    c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_NAME)),
                    c.getInt(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_EXPECTED_TIME)),
                    c.getInt(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_DONE_TIME)));

            subject.addWorkEntryList(fetchWorkEntryListForSubject(subject));

            Log.d("DBHelper", "Subject " + subject.getName() + ": " + subject.getLogEntries().size());

            list.add(subject);
            c.moveToNext();
        }

        Log.d("DBHelper", "Size of list: " + list.size());

        return list;
    }

    public ArrayList<WorkEntry> fetchWorkEntryListForSubject(Subject subject) {
        ArrayList<WorkEntry> list = new ArrayList<WorkEntry>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + FeederReaderContract.FeedEntry.TABLE_WORKENTRIES + " where " + FeederReaderContract.FeedEntry.WORKENTRY_SUBJECT + " = " + subject.getID(), null);
        c.moveToFirst();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);

        while(!c.isAfterLast()) {
            try {
                WorkEntry wE = new WorkEntry(
                        dateFormat.parse(c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.WORKENTRY_START_DATE))),
                        dateFormat.parse(c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.WORKENTRY_END_DATE))),
                        dateFormat.parse(c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.WORKENTRY_START_TIME))),
                        dateFormat.parse(c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.WORKENTRY_END_TIME))),
                        c.getLong(c.getColumnIndex(FeederReaderContract.FeedEntry.WORKENTRY_WORK_TIME)), 0);

                list.add(wE);
                c.moveToNext();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return list;
    }

    public Subject getSubject(long ID) {

        Subject subject;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + FeederReaderContract.FeedEntry.TABLE_SUBJECTS + " where " + FeederReaderContract.FeedEntry.SUBJECT_COLUMN_ID + " = " + ID, null);

        c.moveToFirst();

        subject = new Subject(
                c.getLong(c.getColumnIndex(FeederReaderContract.FeedEntry._ID)),
                c.getString(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_NAME)),
                c.getInt(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_EXPECTED_TIME)),
                c.getInt(c.getColumnIndex(FeederReaderContract.FeedEntry.SUBJECT_COLUMN_DONE_TIME)));

        subject.addWorkEntryList(fetchWorkEntryListForSubject(subject));

        return subject;
    }

    public long removeSubject(long ID) {
        SQLiteDatabase db = getWritableDatabase();
        Log.d("DBHelper", "Removing " + ID);
        return db.delete(FeederReaderContract.FeedEntry.TABLE_SUBJECTS, FeederReaderContract.FeedEntry.SUBJECT_COLUMN_ID + " = " + ID, null);
    }

    public void cleanDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + FeederReaderContract.FeedEntry.TABLE_SUBJECTS);
        db.execSQL("DELETE FROM " + FeederReaderContract.FeedEntry.TABLE_WORKENTRIES);
        db.close();
    }

}
