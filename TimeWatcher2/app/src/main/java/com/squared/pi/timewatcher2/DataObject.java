package com.squared.pi.timewatcher2;

import java.util.Date;
import java.util.List;

/**
 * Created by Theodor on 10.05.2017.
 */

public class DataObject {
    private long ID;
    private String subjectName;
    private int subjectEstimated;
    private long subjectDone;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getSubjectEstimated() {
        return subjectEstimated;
    }

    public void setSubjectEstimated(int subjectEstimated) {
        this.subjectEstimated = subjectEstimated;
    }

    public long getSubjectDone() {
        return subjectDone;
    }

    public void setSubjectDone(long subjectDone) {
        this.subjectDone = subjectDone;
    }

    public long getID() {
        return this.ID;
    }

    DataObject (long ID, String name, int estimated, long done){
        this.ID = ID;
        subjectName = name;
        subjectEstimated = estimated;
        subjectDone = done;
    }
}