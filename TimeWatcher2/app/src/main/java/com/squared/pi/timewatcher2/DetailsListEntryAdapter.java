package com.squared.pi.timewatcher2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Theodor on 17.06.2017.
 */

public class DetailsListEntryAdapter extends ArrayAdapter<WorkEntry> implements View.OnClickListener {

    private Context context;
    private ArrayList<WorkEntry> data;

    private static class ViewHolder {
        TextView startDate;
        TextView endDate;
        TextView startTime;
        TextView endTime;
        TextView hours;
        TextView message;
    }


    public DetailsListEntryAdapter(ArrayList<WorkEntry> data, Context context) {
        super(context, R.layout.subject_details_list_element, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public WorkEntry getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WorkEntry wE = getItem(position);

        ViewHolder holder;

        final View result;

        if(convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.subject_details_list_element, parent, false);

            holder.startDate = (TextView) convertView.findViewById(R.id.subject_details_start_date_value);
            holder.endDate = (TextView) convertView.findViewById(R.id.subject_details_end_date_value);
            holder.startTime= (TextView) convertView.findViewById(R.id.subject_details_start_time_value);
            holder.endTime = (TextView) convertView.findViewById(R.id.subject_details_end_time_value);
            holder.hours = (TextView) convertView.findViewById(R.id.subject_details_hours_value);
            holder.message = (TextView) convertView.findViewById(R.id.subject_details_message_value);


            result = convertView;
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY);
        DateFormat timeFormatter = new SimpleDateFormat("HH:mm",Locale.GERMANY);

        holder.startDate.setText(dateFormatter.format(data.get(position).getStartDate()));
        holder.endDate.setText(dateFormatter.format(data.get(position).getEndDate()));
        holder.startTime.setText(timeFormatter.format(data.get(position).getStartTime()));
        holder.endTime.setText(timeFormatter.format(data.get(position).getEndTime()));
        holder.hours.setText((data.get(position).getWork() / 60) + "");
        holder.message.setText(data.get(position).getMessage());




        return convertView;
    }

    @Override
    public void onClick(View v) {

    }
}
