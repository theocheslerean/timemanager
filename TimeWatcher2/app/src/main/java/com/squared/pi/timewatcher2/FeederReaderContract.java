package com.squared.pi.timewatcher2;

import android.provider.BaseColumns;

/**
 * Created by Theodor on 14.05.2017.
 */

public class FeederReaderContract {

    private FeederReaderContract() {}

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_SUBJECTS = "Subjects";
        public static final String SUBJECT_COLUMN_ID = "_ID";
        public static final String SUBJECT_COLUMN_NAME = "name";
        public static final String SUBJECT_COLUMN_EXPECTED_TIME = "exp_time";
        public static final String SUBJECT_COLUMN_DONE_TIME = "done_time";


        public static final String TABLE_WORKENTRIES = "WorkEntries";
        public static final String WORKENTRY_COLUMN_ID = "_ID";
        public static final String WORKENTRY_START_DATE = "start_date";
        public static final String WORKENTRY_END_DATE = "end_date";
        public static final String WORKENTRY_START_TIME = "start_time";
        public static final String WORKENTRY_END_TIME = "end_time";
        public static final String WORKENTRY_WORK_TIME = "work_time";
        public static final String WORKENTRY_MODIFIED = "modified";
        public static final String WORKENTRY_MESSAGE = "message";
        public static final String WORKENTRY_SUBJECT = "subject";

    }


}
