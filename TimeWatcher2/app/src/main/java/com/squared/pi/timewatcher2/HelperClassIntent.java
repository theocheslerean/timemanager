package com.squared.pi.timewatcher2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Theodor on 30.05.2017.
 */

public class HelperClassIntent implements Parcelable {

    Subject subject;



    protected HelperClassIntent(Parcel in) {
    }

    public static final Creator<HelperClassIntent> CREATOR = new Creator<HelperClassIntent>() {
        @Override
        public HelperClassIntent createFromParcel(Parcel in) {
            return new HelperClassIntent(in);
        }

        @Override
        public HelperClassIntent[] newArray(int size) {
            return new HelperClassIntent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
