package com.squared.pi.timewatcher2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "MainActivity";

    private ArrayList<Subject> subjects;

    private DBHelper db;

    private String helpText;
    private String aboutText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Activities");

        helpText = "Bla bla bla bla bla bla bla bla bla bla bla bla bla bla";
        aboutText = "PiSquared";

        db = new DBHelper(this);
//        db.cleanDB();

//        Log.d("MainActivity", "db is null: " + (db == null));
//
//        Subject s1 = new Subject("Augmented Reality", 100, 60);
//        Subject s2 = new Subject("IDP", 300, 50);
//        Subject s3 = new Subject("Deep Learning", 150, 140);
//        Subject s4 = new Subject("Praktikum", 300, 30);
//
//        Log.d("MainActivity", "Number of subjects before insert: " + db.getAllSubjects().size());
//
//        db.insertSubject(s1);
//        db.insertSubject(s2);
//        db.insertSubject(s3);
//        db.insertSubject(s4);
//
//        Log.d("MainActivity", "Number of subjects after insert: " + db.getAllSubjects().size());

        this.subjects = new ArrayList<Subject>();
        this.subjects = db.getAllSubjects();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyRecyclerViewAdapter(subjects);
        mRecyclerView.setAdapter(mAdapter);

        // Code to Add an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).addItem(obj, index);

        // Code to remove an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).deleteItem(index);
    }


    @Override
    protected void onResume() {
        super.onResume();
        subjects.clear();
        subjects.addAll(db.getAllSubjects());
        ((MyRecyclerViewAdapter) mAdapter).setOnItemClickListener(new MyRecyclerViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
//                Intent intent = new Intent(MainActivity.this, SubjectActivity.class);
//                intent.putExtra("ID", subjects.get(position).getID());
//                startActivity(intent);
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.main_new: addDummySubject(); return true;
            case R.id.main_help: showHelp(); return true;
            case R.id.main_about: showAbout(); return true;
            case R.id.main_exit: endAll(); return true;
            default:
        }
        return false;
    }

    private void addDummySubject() {
        Subject subject = new Subject("New Subject", 150, 0);
        long id = db.insertSubject(subject);
        Log.d("MainActivity", "Inserted " + id);
        subject.setID(id);
        Log.d("MainActivity", "Before: subjects.size() = " + subjects.size());
        subjects.add(subject);
        Log.d("MainActivity", "After: subjects.size() = " + subjects.size());
        //mAdapter.notifyItemRangeInserted(0, subjects.size()-1);
        mAdapter.notifyItemInserted(subjects.size()-1);
        mAdapter.notifyDataSetChanged();
    }

    private void showHelp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Help");
        builder.setMessage(helpText);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAbout() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//
//        builder.setTitle("About");
//        builder.setMessage(aboutText);
//
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.show();

        Intent intent = new Intent(getApplicationContext(), TestPlot.class);
        intent.putExtra("ID", subjects.get(0).getID());
        startActivity(intent);


    }

    private void endAll() {

        subjects = db.getAllSubjects();

        db.cleanDB();
        Log.d("MainActivity", "Number of subject: " + subjects.size());
        for(Subject subject : subjects) {
            db.insertSubject(subject);
            for(WorkEntry wE : subject.getLogEntries()) {
                db.insertWorkEntry(subject, wE);
            }
            Log.d("MainActivity", subject.getName() + " has " + subject.getLogEntries().size() + " log entries");
        }

        Toast.makeText(this.getApplicationContext(), "" + db.getAllSubjects().size(), Toast.LENGTH_SHORT).show();

        finish();
    }


}

