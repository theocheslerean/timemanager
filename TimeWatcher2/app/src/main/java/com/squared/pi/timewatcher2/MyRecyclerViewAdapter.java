package com.squared.pi.timewatcher2;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Theodor on 10.05.2017.
 */

public class MyRecyclerViewAdapter extends RecyclerView
        .Adapter<MyRecyclerViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Subject> mDataset;
    private static MyClickListener myClickListener;

    private RecyclerView recyclerView;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {

        //ProgressBar pB;
        TextView subjectname;
        TextView lastmodified;
        TextView lastlog;
        EditText subjecteditname;
        TextView subjectProgressText;
        CircularProgressBar subjectProgressBar;
        ImageButton menuButton;

        Button openButton;
        Button detailsButton;
        //ImageView image;

        public DataObjectHolder(View itemView) {
            super(itemView);
            subjectname = (TextView) itemView.findViewById(R.id.subject_name);
            subjecteditname = (EditText) itemView.findViewById(R.id.subject_edit_name);
            lastmodified = (TextView) itemView.findViewById(R.id.subject_last_modified);
            lastlog = (TextView) itemView.findViewById(R.id.subject_last_log);
            subjectProgressText = (TextView) itemView.findViewById(R.id.subject_progress_text);
            subjectProgressBar = (CircularProgressBar) itemView.findViewById(R.id.subject_progress_bar);
            //pB = (ProgressBar) itemView.findViewById(R.id.subject_progress);
            menuButton = (ImageButton) itemView.findViewById(R.id.subject_menu);

            openButton = (Button) itemView.findViewById(R.id.open_subject_log);
            detailsButton = (Button) itemView.findViewById(R.id.open_subject_details);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public MyRecyclerViewAdapter(ArrayList<Subject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subject_card_listelement, parent, false);

        recyclerView = (RecyclerView) parent;
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
        holder.subjectname.setText(mDataset.get(position).getName());
        int prg = 0;
        if(mDataset.get(position).getTarget() != 0) {
            prg = (int) Math.floor(((mDataset.get(position).getCurrent() / 60) * 100) / mDataset.get(position).getTarget());
        }



        //holder.subjectProgressText.setText(prg + " %");
        holder.subjectProgressText.setText(mDataset.get(position).getCurrent() / 60 + "/\n" + mDataset.get(position).getTarget());
        holder.subjectProgressBar.setProgress(prg);

        holder.subjectProgressBar.setColor(interpolate(prg));
        holder.subjectProgressText.setTextColor(Color.parseColor("#404040"));


        //holder.subjectProgress.setBackgroundColor(interpolate(Color.parseColor("#ff0000"), Color.parseColor("#00ff00"), prg));
        //holder.pB.setProgress(mDataset.get(position).getSubjectDone());

        holder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(v, position);
            }
        });

        holder.openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSubject(position);
            }
        });

        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDetails(position);
            }
        });

        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY);
        Date modif = mDataset.get(position).getLastModified();

        if(modif != null) {
            holder.lastmodified.setText("Last worked on " + dateFormatter.format(modif));
        }
        else {
            holder.lastmodified.setText("No work done yet");
        }

    }

    private int interpolate(int c1, int c2, int value) {
        Color color = new Color();
        c1 = c1 - ((int) (2.55 * value));
        c2 = ((int) (2.55 * value));
        return Color.rgb(c1,c2,0);
    }

    private int interpolate(int value) {
        Color color = new Color();
        if(value >= 0 && value < 33) {
            return Color.rgb(255,0,0);
        }
        else if(value >= 33 && value < 66) {
            return Color.rgb(255,255,0);
        }
        else if(value >= 66 && value <= 100) {
            return Color.rgb(0,255,0);
        }
        return 0;
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.subject_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));
        popup.show();
    }

    private class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        int position;

        public MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch(item.getItemId()) {
                case R.id.menu_edit: editItem(position);return true;
                case R.id.menu_delete: deleteItem(position);return true;
                default:
            }
            return false;
        }
    }

    public void addItem(Subject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
        notifyDataSetChanged();
    }

    public void deleteItem(int index) {
        Log.d("MyRecyclerViewAdapter", "Before: index= " + index + ", mDataset.size()=" + mDataset.size());
        DBHelper db = new DBHelper(recyclerView.getContext());
        db.removeSubject(mDataset.get(index).getID());
        mDataset.remove(index);
        notifyItemRemoved(index);
        notifyDataSetChanged();
        Log.d("MyRecyclerViewAdapter", "After: index= " + index + ", mDataset.size()=" + mDataset.size());
    }

    public void editItem(int index) {

        final int position = index;

        final TextView tV = (TextView) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_name);
        final EditText eT = (EditText) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_edit_name);
        //final TextView prg = (TextView) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_progress);
        final RelativeLayout prg = (RelativeLayout) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_progress_layout);

        final Button btn_ok = (Button) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_ok);
        final ImageButton btn_menu = (ImageButton) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.subject_menu);

        final CardView cardView = (CardView) recyclerView.findViewHolderForAdapterPosition(index).itemView.findViewById(R.id.card_view);
        cardView.setClickable(false);

        tV.setVisibility(View.GONE);
        eT.setVisibility(View.VISIBLE);
        eT.setText(tV.getText());
        btn_menu.setVisibility(View.GONE);
        btn_ok.setVisibility(View.VISIBLE);



        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) prg.getLayoutParams();
        params.removeRule(RelativeLayout.BELOW);
        params.addRule(RelativeLayout.BELOW, R.id.header);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String newName = eT.getText().toString().trim();

                tV.setVisibility(View.VISIBLE);
                eT.setVisibility(View.GONE);
                btn_menu.setVisibility(View.VISIBLE);
                btn_ok.setVisibility(View.GONE);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) prg.getLayoutParams();
                params.removeRule(RelativeLayout.BELOW);
                params.addRule(RelativeLayout.BELOW, R.id.header);

                cardView.setClickable(true);


                Subject dO = mDataset.get(position);
                if(newName.length() > 0) {
                    dO.setName(eT.getText().toString());
                    notifyDataSetChanged();
                    Log.d("MyRecyclerViewAdapter", "editItem(): index= " + position + ", " + dO.getName());
                }

                DBHelper db = new DBHelper(recyclerView.getContext());
                db.updateSubject(dO);


            }
        });



    }

    public void openDetails(int index) {
        Intent intent = new Intent(recyclerView.getContext(), SubjectDetailsActivity.class);
        intent.putExtra("ID", mDataset.get(index).getID());
        recyclerView.getContext().startActivity(intent);
    }


    public void openSubject(int index) {
        Intent intent = new Intent(recyclerView.getContext(), SubjectActivity.class);
        intent.putExtra("ID", mDataset.get(index).getID());
        recyclerView.getContext().startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}