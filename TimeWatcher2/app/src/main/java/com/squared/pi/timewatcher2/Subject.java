package com.squared.pi.timewatcher2;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Theodor on 10.05.2017.
 */

public class Subject {
    private long ID;
    private String name;
    private long target;
    //in minutes
    private long current;

    private ArrayList<WorkEntry> logEntries;

    Subject(String name, long expectedWorkHours, long currentWorkHours) {
        this.name = name;
        this.target = expectedWorkHours;
        this.current = currentWorkHours;
        this.logEntries = new ArrayList<WorkEntry>();
    }

    Subject(long ID, String name, long expectedWorkHours, long currentWorkHours) {
        this.ID = ID;
        this.name = name;
        this.target = expectedWorkHours;
        this.current = currentWorkHours;
        this.logEntries = new ArrayList<WorkEntry>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public long getTarget() { return this.target; }

    public long getCurrent() {

        int sum = 0;

        for(WorkEntry wE : this.logEntries) {
            sum += wE.getWork();
        }

        return sum;
    }

    public long getID() {
        return this.ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public void addNewEntry(WorkEntry wE) {

        this.logEntries.add(wE);

    }


    public void addWorkEntryList(ArrayList<WorkEntry> list) {
        for(WorkEntry element : list) {
            this.addNewEntry(element);
        }
    }

    public Date getLastModified() {
        if(this.logEntries.size() != 0) {
            return this.logEntries.get(this.logEntries.size() - 1).getModified();
        }
        return null;
    }

    public ArrayList<WorkEntry> getLogEntries() {
        return this.logEntries;
    }

}
