package com.squared.pi.timewatcher2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.icu.util.DateInterval;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SubjectActivity extends AppCompatActivity implements View.OnClickListener{

    private Subject subject;

    private DatePickerDialog startDatePicker;
    private TimePickerDialog startTimePicker;
    private DatePickerDialog endDatePicker;
    private TimePickerDialog endTimePicker;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;

    TextView startDate;
    TextView startTime;
    TextView endDate;
    TextView endTime;
    TextView hours;
    TextView notes;
    Button logWork;

    private Date startDateValue;
    //private Date endDateValue;
    private Date startTimeValue;
    private Date endTimeValue;
    private long difference;
    //private int pause;

    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new DBHelper(this);

        Intent intent = getIntent();

        subject = db.getSubject(intent.getLongExtra("ID", -1));

        Log.d("SubjectActivity", "Number of Workentries: " + subject.getLogEntries().size());

        setupView();
        setupDialogs();

    }

    private void setupView() {
        startDate = (TextView) findViewById(R.id.activity_subject_start_date_value);
        startTime = (TextView) findViewById(R.id.activity_subject_start_time_value);
        //endDate = (TextView) findViewById(R.id.activity_subject_end_date_value);
        endTime = (TextView) findViewById(R.id.activity_subject_end_time_value);
        hours = (TextView) findViewById(R.id.activity_subject_time_value);
        notes = (TextView) findViewById(R.id.activity_subject_notes_value);
        logWork = (Button) findViewById(R.id.activity_subject_log_work);

        startDate.setOnClickListener(this);
        startTime.setOnClickListener(this);
        //endDate.setOnClickListener(this);
        endTime.setOnClickListener(this);
        logWork.setOnClickListener(this);
    }

    private void setupDialogs() {

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY);
        timeFormatter = new SimpleDateFormat("HH:mm",Locale.GERMANY);

        startDate.setText(dateFormatter.format(new Date()));
        startTime.setText(timeFormatter.format(new Date()));
        startDateValue = new Date();
        startTimeValue = new Date();

        //endDate.setText("Fill in Date");
        endTime.setText("Fill in Time");


        Calendar newCalendar = Calendar.getInstance();

        startDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                startDateValue = newDate.getTime();
                startDate.setText(dateFormatter.format(startDateValue));
                updateHours();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        startTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                c.set(Calendar.MINUTE,minute);
                startTimeValue = c.getTime();
                startTime.setText(timeFormatter.format(startTimeValue));
                updateHours();
            }
        },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), true);

//        endDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                endDateValue = newDate.getTime();
//                endDate.setText(dateFormatter.format(endDateValue));
//                updateHours();
//            }
//
//        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        endTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                c.set(Calendar.MINUTE,minute);
                endTimeValue = c.getTime();
                endTime.setText(timeFormatter.format(endTimeValue));
                updateHours();
            }
        },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), true);
    }


    @Override
    public void onClick(View view) {

        if(view == startDate) {
            startDatePicker.show();
        } else if(view == startTime) {
            startTimePicker.show();
        } else if(view == endTime) {
            endTimePicker.show();
        } else if(view == notes) {
            endTimePicker.show();
        } else if(view == logWork) {
            logWorkAndFinish();
        }
    }

    private void logWorkAndFinish() {

        Log.d("SubjectActivity", "Saving WorkEntry to Subject");

        //subject.addNewEntry();

        db.insertWorkEntry(subject, new WorkEntry(startDateValue, startTimeValue, startDateValue, endTimeValue, difference, 0));

        finish();
    }

    private void updateHours() {
        difference = 0;
        if(startDateValue != null && startTimeValue != null && endTimeValue != null) {
            //long tmpDate = endDateValue.getTime() - startDateValue.getTime();
            long tmpTime = endTimeValue.getTime() - startTimeValue.getTime();
            //In minutes
            difference =  (tmpTime / (60 * 1000));
        }
        hours.setText((difference / 60) + ":" + (difference % 60));
    }

}
