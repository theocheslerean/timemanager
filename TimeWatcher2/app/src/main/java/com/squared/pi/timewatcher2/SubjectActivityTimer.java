package com.squared.pi.timewatcher2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SubjectActivityTimer extends AppCompatActivity {


    private Subject subject;

    private Button startButton;
    private Button pauseButton;
    private Button resetButton;

    private TextView timerValue;

    private long startTime = 0L;

    private Handler customHandler = new Handler();

    private boolean alreadyStartedOnce;

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;

    long updatedTime = 0L;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_timer);

        alreadyStartedOnce = false;

        DBHelper db = new DBHelper(this);

        Intent intent = getIntent();

        subject = db.getSubject(intent.getLongExtra("ID", -1));

        getSupportActionBar().setTitle(subject.getName());

        timerValue = (TextView) findViewById(R.id.timer);

        startButton = (Button) findViewById(R.id.start_image);
        pauseButton = (Button) findViewById(R.id.stop_image);
        resetButton = (Button) findViewById(R.id.reset_image);

        pauseButton.setEnabled(false);
        resetButton.setEnabled(false);

        startButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {


                startTime = SystemClock.uptimeMillis();


                customHandler.postDelayed(updateTimerThread, 0);

                startButton.setEnabled(false);
                pauseButton.setEnabled(true);
                resetButton.setEnabled(false);

                timerValue.setTextColor(Color.GREEN);

                startSubject();

            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                customHandler.removeCallbacks(updateTimerThread);

                timeSwapBuff += timeInMilliseconds;

                startButton.setEnabled(true);
                pauseButton.setEnabled(false);
                resetButton.setEnabled(true);

                timerValue.setTextColor(Color.RED);

                stopSubject();

            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SubjectActivityTimer.this);

                builder.setTitle("Reset Timer");
                builder.setMessage("Are you sure?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        updatedTime = 0L;
                        timeSwapBuff = 0L;
                        timerValue.setText("00:00:00");
                        timerValue.setTextColor(Color.GRAY);

                        pauseButton.setEnabled(false);
                        resetButton.setEnabled(false);

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        });

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int mins = (int) (updatedTime / (60 * 1000));
            int hours = (int) (updatedTime / (60 * 60 * 1000));
            //int mins = secs / 60;
            int secs = (int) (updatedTime / 1000);
            //int milliseconds = (int) (updatedTime % 1000);
            String h = hours < 10 ? "0" + hours : "" + hours;
            String m = mins < 10 ? "0" + mins : "" + mins;
            String s = secs < 10 ? "0" + secs : "" + secs;
            timerValue.setText(h + ":" + m + ":" + s);
            customHandler.postDelayed(this, 0);
        }

    };


    public void startSubject() {



    }

    public void stopSubject() {



    }

}
