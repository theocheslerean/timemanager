package com.squared.pi.timewatcher2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SubjectDetailsActivity extends AppCompatActivity {

    private Subject subject;
    private DBHelper db;
    private DetailsListEntryAdapter adapter;
    private ArrayList<WorkEntry> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        db = new DBHelper(this);

        Intent intent = getIntent();

        subject = db.getSubject(intent.getLongExtra("ID", -1));




        Log.d("SubjectDetailsActivity", "Size of Log list: " + subject.getLogEntries().size());


        ListView lV = (ListView) findViewById(R.id.subject_details_list);

        data = new ArrayList<WorkEntry>();
        data.addAll(subject.getLogEntries());

        adapter = new DetailsListEntryAdapter(data, getApplicationContext());
        lV.setAdapter(adapter);
        lV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WorkEntry wE = data.get(position);

                Snackbar.make(view, "Pressed " + wE.getStartDate().toString(), Snackbar.LENGTH_LONG).setAction("No action", null).show();

            }
        });



    }

}
