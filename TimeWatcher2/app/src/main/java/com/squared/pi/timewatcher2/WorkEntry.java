package com.squared.pi.timewatcher2;

import java.util.Date;

/**
 * Created by Theodor on 03.06.2017.
 */

public class WorkEntry {

    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private Date modified;


    private String message;

    public WorkEntry(Date startDate, Date startTime, Date endDate, Date endTime, long minutes, int pause) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.work = minutes;
        this.pause = pause;
        this.modified = new Date();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endDate = endTime;
    }

    public long getWork() {
        return work;
    }

    public void setWork(long work) {
        this.work = work;
    }

    public long getPause() {
        return pause;
    }

    public void setPause(long pause) {
        this.pause = pause;
    }

    public Date getModified() {
        return this.modified;
    }

    public void setModified() {
        this.modified = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    private long work;
    private long pause;


}
